# CMake generated Testfile for 
# Source directory: /home/lyan/Téléchargements/SimGrid-3.25/examples/smpi/replay_multiple
# Build directory: /home/lyan/Téléchargements/SimGrid-3.25/examples/smpi/replay_multiple
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(smpi-replay-multiple "/usr/bin/python3" "/home/lyan/Téléchargements/SimGrid-3.25/bin/tesh" "--ignore-jenkins" "--setenv" "srcdir=/home/lyan/Téléchargements/SimGrid-3.25/examples/smpi/replay_multiple" "--setenv" "bindir=/home/lyan/Téléchargements/SimGrid-3.25/examples/smpi/replay_multiple" "--cd" "/home/lyan/Téléchargements/SimGrid-3.25/examples/smpi/replay_multiple" "/home/lyan/Téléchargements/SimGrid-3.25/examples/smpi/replay_multiple/replay_multiple.tesh")
set_tests_properties(smpi-replay-multiple PROPERTIES  _BACKTRACE_TRIPLES "/home/lyan/Téléchargements/SimGrid-3.25/tools/cmake/Tests.cmake;49;ADD_TEST;/home/lyan/Téléchargements/SimGrid-3.25/examples/smpi/replay_multiple/CMakeLists.txt;6;ADD_TESH;/home/lyan/Téléchargements/SimGrid-3.25/examples/smpi/replay_multiple/CMakeLists.txt;0;")
