# Workflows simulations' explanation

## Structure of the project

They are many folders in this project, described as following, representing the components of a simgrid study:

* **apps** : this folder we put all the applications, that we what to simulate or to test in simulation
* **plateforms** : contains all the platform files of the simulations (in our case, we will have a same plateform for all the simulations)
* **automatic** : contains files to build platform, and to deploy actors automatically
* **images** : contains images of design of the simulation

## How to run a simulation

if the platform and the application are completed, just do : 

```py
   python3 application_file_path platform_file_path
```

**for example (if you are in this folder):**
```py
   python3 apps/app_completed.py platforms/platform_completed.xml
```