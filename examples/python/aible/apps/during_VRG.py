from simgrid import Actor, Engine, Host, Mailbox, this_actor
import sys, random

disabilities = ['Hand(s)', 'Finger(s)', 'Leg(s)', 'Mouth']

# params
patient_1 = "Lucien1"
fn_1_1 = "cb"+str(random.randint(1,3))+"-"+str(random.randint(1, 15))+".dc1.acloud.com"
cn_1_1 = "cb-"+str(random.randint(1, 10))+".dc2.acloud.com"
fn_2_1 = "cb"+str(random.randint(1,3))+"-"+str(random.randint(1, 15))+".dc1.acloud.com"
cn_2_1 = "cb-"+str(random.randint(1, 10))+".dc2.acloud.com"
# end_params


def post(src, dst, msg):
    mb = Mailbox.by_name(src+"_in")
    this_actor.info("Sending (-> "+dst+")"+" {"+msg+"} <"+src+":"+dst+">")
    mb.put(src+"|"+msg, len(src+"|"+msg))
    this_actor.info("{"+msg+", sent} <"+src+":"+dst+">")

def forward_to_cloud(src, inter, dst):
    mb1 = Mailbox.by_name(src+"_in")
    msg = mb1.get().split("|")[1]
    this_actor.info("Saving (-> "+inter+")"+" {"+msg+"} <"+src+":"+dst+">")
    this_actor.info("Forwarding (-> "+dst+")"+" {"+msg+"} <"+src+":"+dst+">")
    this_actor.info("{"+msg+", forwarded} <"+src+":"+dst+">")

def put(src, inter, dst):
    mb1 = Mailbox.by_name(src+"_in")
    msg = mb1.get().split("|")[1]
    this_actor.info("Saving (-> "+inter+")"+" {"+msg+"} <"+src+":"+dst+">")
    this_actor.info("Forwarding (-> "+dst+")"+" {"+msg+"} <"+src+":"+dst+">")
    mb2 = Mailbox.by_name(dst+"_out")
    mb2.put(src+"|"+msg, len(src+"|"+msg))
    this_actor.info("{"+msg+", forwarded} <"+src+":"+dst+">")

def get(inter, dst):
    mb = Mailbox.by_name(dst+"_out")
    msg_g = mb.get()
    src = msg_g.split("|")[0]
    msg = msg_g.split("|")[1]
    this_actor.info("Receiving (<- "+inter+")"+" {"+msg+"} <"+src+":"+dst+">")

if __name__ == '__main__':

    # Here comes the main function of your program
    # When your program starts, you have to first start a new simulation engine, as follows
    e = Engine(sys.argv)

    # Then you should load a platform file, describing your simulated platform
    e.load_platform(sys.argv[1])

    # actors_creation
    msg = "param1:"+str(random.randint(0, 10))+" param2:"+str(random.randint(0, 10))+" param3:"+str(random.randint(0, 10))+" param4:"+str(random.randint(0, 10))+" param5:"+str(random.randint(0, 10))
    msg_corr = "param1:"+str(random.randint(0, 10))+" param2:"+str(random.randint(0, 10))+" param3:"+str(random.randint(0, 10))+" param4:"+str(random.randint(0, 10))+" param5:"+str(random.randint(0, 10))
    Actor.create("exo1", Host.by_name("exo1"), post, "exo1", fn_1_1, msg)
    Actor.create("exo1", Host.by_name("exo1"), post, "exo1", cn_1_1, msg)
    Actor.create(fn_1_1, Host.by_name(fn_1_1), put, "exo1", fn_1_1, "kine_hri1")
    Actor.create(cn_1_1, Host.by_name(fn_1_1), put, "exo1", cn_1_1, "doc_hri1")
    Actor.create("kine_hri1", Host.by_name("kine_hri1"), get, fn_1_1, "kine_hri1")
    Actor.create("doc_hri1", Host.by_name("doc_hri1"), get, cn_1_1, "doc_hri1")
    if random.randint(0, 1) == 1 :
        Actor.create("kine_hri1", Host.by_name("kine_hri1"), post, "kine_hri1", fn_2_1, msg_corr)
        Actor.create(fn_2_1, Host.by_name(fn_2_1), forward_to_cloud, "kine_hri1", fn_2_1, cn_2_1)
    # end_actors_creation

    # Once every actors are started in the engine, the simulation can start
    e.run()
