from simgrid import Actor, Engine, Host, Mailbox, this_actor
import sys, random

disabilities = ['Hand(s)', 'Finger(s)', 'Leg(s)', 'Mouth']

# params
patient_1 = "Lucien1"
server1_1 = "cb-"+str(random.randint(1, 10))+".dc2.acloud.com"
server2_1 = "cb-"+str(random.randint(1, 10))+".dc2.acloud.com"
#server1_1 = "cb1-"+str(random.randint(1, 10))+".dc1.acloud.com"
#server2_1 = "cb1-"+str(random.randint(1, 10))+".dc1.acloud.com"
# end_params

def post_emg_data(src, dst):
    mailbox = Mailbox.by_name(src+"_in")
    msg = src+"|"+disabilities[random.randint(0, 3)]+":"+str(random.randint(0, 10))+":"+str(random.randint(0, 10))+":"+str(random.randint(0, 10))+":"+str(random.randint(0, 10))+":"+str(random.randint(0, 10))
    this_actor.info("Sending (-> "+dst+")"+" {"+msg+"} <"+src+":"+dst+">")
    mailbox.put(msg, len(msg))
    this_actor.info("{"+msg+", transmitted} <"+src+":"+dst+">")

def confirm_record(src, cloud_host, storage):
    mb = Mailbox.by_name(src+"_in")
    msg = mb.get().split("|")[1]
    this_actor.info("Receiving: (<- "+src+")"+" {"+msg+" related to "+src+"} <"+src+":"+cloud_host+">")
    this_actor.info("Saving: (-> "+cloud_host+")"+" {"+msg+" related to "+src+"} <"+src+":"+cloud_host+">")
    mb2 = Mailbox.by_name(storage+"_out")
    mb2.put(src+"|"+msg, len(src+"|"+msg))
    this_actor.info("{"+msg+", saved} <"+src+":"+cloud_host+">")

def get_data(src, dst, patient):
    mb = Mailbox.by_name(dst+"_out")
    msg_g = mb.get()
    msg = msg_g.split("|")[1]
    this_actor.info("Receiving: (<- "+src+")"+" {"+msg+" related to "+patient+"} <"+src+":"+dst+">")
    
def post_vrgames(src, dst, patient):
    vrgames = ""
    nbvrg = random.randint(1, 5)
    nbvrglist = [] 
    for i in range(nbvrg):
        n = random.randint(1, nbvrg)
        if (not (n in nbvrglist)):
            nbvrglist.append(n)
    for i in range(len(nbvrglist)):
        if (i == len(nbvrglist)-1):
            vrgames+=("VRG"+str(nbvrglist[i]))
        else:
            vrgames+=("VRG"+str(nbvrglist[i])+":")
    this_actor.info("Sending (-> "+dst+")"+" {"+vrgames+" related to "+patient+"} <"+src+":"+dst+">")
    mb2 = Mailbox.by_name(src+"_in")
    mb2.put(src+"|"+vrgames, len(src+"|"+vrgames))
    this_actor.info("{"+vrgames+", transmitted}")

def select_then_post_vrgame(src, dst, patient, storage):
    mb = Mailbox.by_name(dst+"_out")
    msg_g = mb.get()
    msg = msg_g.split("|")[1]
    this_actor.info("Receiving: (<- "+src+")"+" {"+msg+" related to "+patient+"} <"+msg_g.split("|")[0]+":"+dst+">")
    vrgames = msg.split("|")
    ivg = random.randint(0, len(vrgames)-1)
    this_actor.info("Sending (-> "+storage+")"+" {"+vrgames[ivg]+" related to "+patient+"} <"+dst+":"+storage+">")
    mb2 = Mailbox.by_name(storage+"_out")
    mb2.put(dst+"|"+vrgames[ivg], len(dst+"|"+vrgames[ivg]))



if __name__ == '__main__':

    # Here comes the main function of your program
    # When your program starts, you have to first start a new simulation engine, as follows
    e = Engine(sys.argv)

    # Then you should load a platform file, describing your simulated platform
    e.load_platform(sys.argv[1])

    # actors_creation
    Actor.create(patient_1, Host.by_name("emg1"), post_emg_data, "emg1", server1_1)
    Actor.create(server1_1, Host.by_name(server1_1), confirm_record, "emg1", server1_1, "doc_hri1")
    Actor.create("Doctor1", Host.by_name("doc_hri1"), get_data, server1_1, "doc_hri1", patient_1)
    Actor.create("Doctor1", Host.by_name("doc_hri1"), post_vrgames, "doc_hri1", server2_1, patient_1)
    Actor.create(server2_1, Host.by_name(server2_1), confirm_record, "doc_hri1", server2_1, "kine_hri1")
    Actor.create("Kine1", Host.by_name("kine_hri1"), select_then_post_vrgame, server2_1, "kine_hri1", patient_1, "exo1")
    Actor.create("Exoskeleton1", Host.by_name("exo1"), get_data, "kine_hri1", "exo1", patient_1)
    # end_actors_creation

    # Once every actors are started in the engine, the simulation can start
    e.run()
