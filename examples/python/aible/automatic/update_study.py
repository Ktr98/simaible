import os, sys, random

npatient = int(sys.argv[1])
nkine = int(sys.argv[2])
ndoctor = int(sys.argv[3])

"""with open("apps/app_completed.py",'r') as fr:
    ch = fr.read()"""

# p = position du premier couple d'octets de valeurs \r\n
# p+2 est la position du premier octet juste après le couple \r\n
#p = ch.find('# end_actors_creation')
code_params=''
code_app=''
code_patients=''
code_doctors=''
code_links=''
code_zone_routes=''

for i in range(1, npatient+1):
	kine = random.randint(1, nkine)
	doctor = random.randint(1, ndoctor)
	code_params+='patient_'+str(i)+' = "Lucien'+str(i)+'"'+'\nserver'+str(i)+'_'+str(i)+' = "cb-"+str(random.randint(1, 10))+".dc2.acloud.com"'+'\nserver2_'+str(i)+' = "cb-"+str(random.randint(1, 10))+".dc2.acloud.com"\n'
	code_app+='Actor.create(patient_'+str(i)+', Host.by_name("emg'+str(i)+'"), post_emg_data, "emg'+str(i)+'", server'+str(i)+'_'+str(i)+')'+'\n    Actor.create(server'+str(i)+'_'+str(i)+', Host.by_name(server'+str(i)+'_'+str(i)+'), confirm_record, "emg'+str(i)+'", server'+str(i)+'_'+str(i)+', "doc_hri'+str(doctor)+'")'+'\n    Actor.create("Doctor'+str(doctor)+'", Host.by_name("doc_hri'+str(doctor)+'"), get_data, server'+str(i)+'_'+str(i)+', "doc_hri'+str(doctor)+'", patient_'+str(i)+')'+'\n    Actor.create("Doctor'+str(doctor)+'", Host.by_name("doc_hri'+str(doctor)+'"), post_vrgames, "doc_hri'+str(doctor)+'", server2_'+str(i)+', patient_'+str(i)+')'+'\n    Actor.create(server2_'+str(i)+', Host.by_name(server2_'+str(i)+'), confirm_record, "doc_hri'+str(doctor)+'", server2_'+str(i)+', "kine_hri'+str(kine)+'")'+'\n    Actor.create("Kine'+str(kine)+'", Host.by_name("kine_hri'+str(kine)+'"), select_then_post_vrgame, server2_'+str(i)+', "kine_hri'+str(kine)+'", patient_'+str(i)+', "exo'+str(i)+'")'+'\n    Actor.create("Exoskeleton'+str(i)+'", Host.by_name("exo'+str(i)+'"), get_data, "kine_hri'+str(kine)+'", "exo'+str(i)+'", patient_'+str(i)+')\n    '
	code_patients+='<zone id="patient'+str(i)+'" routing="Full">'+'\n      <host id="emg'+str(i)+'" speed="50Mf"/>'+'\n      <host id="exo'+str(i)+'" speed="50Mf"/>'+'\n      <host id="kine_hri'+str(kine)+'" speed="50Mf"/>'+'\n      <router id="gw_patient'+str(i)+'"/>'+'\n      <link id="emg_gw_patient'+str(i)+'" bandwidth="41.279125MBps" latency="59.904us"/>'+'\n      <link id="exo_gw_patient'+str(i)+'" bandwidth="41.279125MBps" latency="59.904us"/>'+'\n      <link id="kine_hri_gw_patient'+str(i)+'" bandwidth="41.279125MBps" latency="59.904us"/>'+'\n      <route src="emg'+str(i)+'" dst="gw_patient'+str(i)+'">'+'\n        <link_ctn id="emg_gw_patient'+str(i)+'"/>'+'\n      </route>'+'\n      <route src="exo'+str(i)+'" dst="gw_patient'+str(i)+'">'+'\n        <link_ctn id="exo_gw_patient'+str(i)+'"/>'+'\n      </route>'+'\n      <route dst="exo'+str(i)+'" src="gw_patient'+str(i)+'">'+'\n        <link_ctn id="exo_gw_patient'+str(i)+'"/>'+'\n      </route>'+'\n      <route src="kine_hri'+str(kine)+'" dst="gw_patient'+str(i)+'">'+'\n        <link_ctn id="kine_hri_gw_patient'+str(i)+'"/>'+'\n      </route>'+'\n      <route dst="kine_hri'+str(kine)+'" src="gw_patient'+str(i)+'">'+'\n        <link_ctn id="kine_hri_gw_patient'+str(i)+'"/>'+'\n      </route>'+'\n      <route dst="kine_hri'+str(kine)+'" src="exo'+str(i)+'">'+'\n        <link_ctn id="kine_hri_gw_patient'+str(i)+'"/>'+'\n        <link_ctn id="exo_gw_patient'+str(i)+'"/>'+'\n      </route>'+'\n    </zone>\n    '
	code_links+='<link id="gw_patient'+str(i)+'_router_AS1_dc1" bandwidth="41.279125MBps" latency="59.904us"/>\n    '
	code_links+='<link id="gw_patient'+str(i)+'_router_AS1_dc2" bandwidth="41.279125MBps" latency="64.904us"/>\n    '
	code_zone_routes+='<zoneRoute src="patient'+str(i)+'" dst="AS0" gw_src="gw_patient'+str(i)+'" gw_dst="router_AS1_dc1">'+'\n      <link_ctn id="gw_patient'+str(i)+'_router_AS1_dc1"/>'+'\n    </zoneRoute>'+'\n    <zoneRoute dst="patient'+str(i)+'" src="AS0" gw_dst="gw_patient'+str(i)+'" gw_src="router_AS1_dc1">'+'\n      <link_ctn id="gw_patient'+str(i)+'_router_AS1_dc1"/>'+'\n    </zoneRoute>\n    ' 
	code_zone_routes+='<zoneRoute src="patient'+str(i)+'" dst="AS1" gw_src="gw_patient'+str(i)+'" gw_dst="router_AS1_dc2">'+'\n      <link_ctn id="gw_patient'+str(i)+'_router_AS1_dc2"/>'+'\n    </zoneRoute>'+'\n    <zoneRoute dst="patient'+str(i)+'" src="AS1" gw_dst="gw_patient'+str(i)+'" gw_src="router_AS1_dc2">'+'\n      <link_ctn id="gw_patient'+str(i)+'_router_AS1_dc2"/>'+'\n    </zoneRoute>\n    '   

for i in range(1, ndoctor+1):
	code_links+='<link id="gw_doctor'+str(i)+'_router_AS1_dc2" bandwidth="41.279125MBps" latency="64.904us"/>\n    '
	code_doctors+='<zone id="doctor'+str(i)+'" routing="Full">'+'\n      <host id="doc_hri'+str(i)+'" speed="50Mf"/>'+'\n      <router id="gw_doctor'+str(i)+'"/>'+'\n      <link id="doc_hri_gw_doctor'+str(i)+'" bandwidth="41.279125MBps" latency="59.904us"/>'+'\n      <route src="doc_hri'+str(i)+'" dst="gw_doctor'+str(i)+'">'+'\n        <link_ctn id="doc_hri_gw_doctor'+str(i)+'"/>'+'\n      </route>'+'\n      <route dst="doc_hri'+str(i)+'" src="gw_doctor'+str(i)+'">'+'\n        <link_ctn id="doc_hri_gw_doctor'+str(i)+'"/>'+'\n      </route>'+'\n    </zone>\n    '
	code_zone_routes+='<zoneRoute src="doctor'+str(i)+'" dst="AS1" gw_src="gw_doctor'+str(i)+'" gw_dst="router_AS1_dc2">'+'\n      <link_ctn id="gw_doctor'+str(i)+'_router_AS1_dc2"/>'+'\n    </zoneRoute>'+'\n    <zoneRoute dst="doctor'+str(i)+'" src="AS1" gw_dst="gw_doctor'+str(i)+'" gw_src="router_AS1_dc2">'+'\n      <link_ctn id="gw_doctor'+str(i)+'_router_AS1_dc2"/>'+'\n    </zoneRoute>\n    '

def complete(file, code, where):
	with open(file,'r') as fr:
	    ch = fr.read()

	# p = position du premier couple d'octets de valeurs \r\n
	# p+2 est la position du premier octet juste après le couple \r\n
	p = ch.find(where)
	with open(file,'w') as fw:
	    fw.write(ch[0:p])
	    fw.write(code)
	    fw.write(ch[p:])
	fr.close()
	fw.close()


complete("../apps/before_VRG.py", code_params, '# end_params')
complete("../apps/before_VRG.py", code_app, '# end_actors_creation')
complete("../platforms/platform_completed.xml", code_patients, '<!-- end_patients_zone -->')
complete("../platforms/platform_completed.xml", code_doctors, '<!-- end_doctors_zone -->')
complete("../platforms/platform_completed.xml", code_links, '<!-- end_links_zone -->')
complete("../platforms/platform_completed.xml", code_zone_routes, '<!-- end_zone_routes -->')
