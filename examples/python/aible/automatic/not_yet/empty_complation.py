import re  


def replace(file_path, pattern, subst):
	regex = re.compile(pattern)
	f = open(file_path, "r+")
	ctn = f.read()
	resultat = regex.findall(ctn)
	print(resultat)


replace("test.xml", "(<\!\-\- patients_zone \-\->)(\s)*[(<(\w)((\s)(\w)=\"(\w)\.(\w)\")+(\/)?>)(<\/(\w)>)]*(\s)*(<\!\-\- end_patients_zone \-\->)", "<!-- patients_zone -->\n<!-- end_patients_zone -->")

#[<>\"=\w\s]*(<!-- end_patients_zone -->)

#'<zone id="patient1" routing="Full">'+'\n      <host id="emg1" speed="50Mf"/>'+'\n      <host id="exo1" speed="50Mf"/>'+'\n      <host id="kine_hri1" speed="50Mf"/>'+'\n      <router id="gw_patient1"/>'+'\n      <link id="emg_gw_patient1" bandwidth="41.279125MBps" latency="59.904us"/>'+'\n      <link id="exo_gw_patient1" bandwidth="41.279125MBps" latency="59.904us"/>'+'\n      <link id="kine_hri_gw_patient1" bandwidth="41.279125MBps" latency="59.904us"/>'+'\n      <route src="emg1" dst="gw_patient1">'+'\n        <link_ctn id="emg_gw_patient1"/>'+'\n      </route>'+'\n      <route src="exo1" dst="gw_patient1">'+'\n        <link_ctn id="exo_gw_patient1"/>'+'\n      </route>'+'\n      <route dst="exo1" src="gw_patient1">'+'\n        <link_ctn id="exo_gw_patient1"/>'+'\n      </route>'+'\n      <route src="kine_hri1" dst="gw_patient1">'+'\n        <link_ctn id="kine_hri_gw_patient1"/>'+'\n      </route>'+'\n      <route dst="kine_hri1" src="gw_patient1">'+'\n        <link_ctn id="kine_hri_gw_patient1"/>'+'\n      </route>'+'\n      <route dst="kine_hri1" src="exo1">'+'\n        <link_ctn id="kine_hri_gw_patient1"/>'+'\n        <link_ctn id="exo_gw_patient1"/>'+'\n      </route>'+'\n    </zone>\n    '

#[(<(\w)+((\s)+(\w)+=\"(\w)+\")+(/)?>)(</(\w)+>)]