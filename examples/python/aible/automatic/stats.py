import os, sys

# first parameter
file_app_path = sys.argv[1]

# second parameter
platform_path = sys.argv[2]

# third parameter 
file_log_path = sys.argv[3]

# parameters after represents the services

# saving the hystory of the execution
os.system("python3 "+file_app_path+" "+platform_path+" > "+file_log_path+" 2>&1")

# building stats
if (len(sys.argv)==4):
	print("There is no service specified to get stats on, go to log.txt, to look for the results o the simulation")
else:
	# the last parameter is the name of the stats file
	os.system("touch "+sys.argv[-1])
	stats = open(sys.argv[-1], "w+")

	stats.write("server, service, start_time\n")

	nb_services = len(sys.argv)-5
	
	for i in range(nb_services):
		os.system("cat "+file_log_path+" > log_"+sys.argv[4+i]+".txt |grep \"[python/INFO] "+sys.argv[4+i]+"\"")
		tmp = open("log_"+sys.argv[4+i]+".txt", "r+")

		for line in tmp:
			first =line.split(" ")
			if(first[3] == sys.argv[4+i]):
				if ("cb" in first[5]):
					server = first[5].split(")")[0]
				else:
					server = (first[0].split(":")[0]).split("[")[1]
			else:
				server = (first[0].split(":")[0]).split("[")[1]
			start_time = first[1].split("]")[0]
			service = sys.argv[4+i]
			if "cb" in server:
				stats.write("\""+server+"\","+"\""+service+"\","+start_time+"\n")

		tmp.close()
		os.system("rm log_"+sys.argv[4+i]+".txt")

	stats.close()
	os.system("rm "+file_log_path)
