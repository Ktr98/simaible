# Building platform, actor's deployment and generate stats

This folder contains files needed to build platform, actors' deployement and to generate stats of communications when the application runs on the platform

it contains 2 main files :

## Building platform and actor's deployment

* **update_study.py** : Which complete the platform xml code, and the actors's deployment in the application's code 
we use it like this : 

```py
   python update_study.py number_of_patient number_of_therapists number_of_doctors
```

**for example (if you are in this folder):**
```py
   python3 update_study.py 2 4 3
```
It will complete :
* in the platform xml file: 
   * 2 zones, each containing  patient's host, exoskeleton's host, therapist's host for which the id is choosen randomly among the 4 given, one router, links and routes used to link those hosts
   * 3 zones, each containing doctor's host, one router, link between both hosts and route to link them
   * 5 links and 10 zoneRoutes, to link patient's zone (2 links, 4 zoneRoutes) and doctor's zone (3 links, 6 zoneRoutes) with the cloud zone 


## Generate statistics

* **stats.py** : used to generate a csv file, show how much a server is requested for a specific service and at a specific time
we use it like this : 

```py
   python stats.py application_file_path platform_file_path log_file_path service1 service2 ... serviceN stats_file_path
```
**NB :** the stats_file_path refere to a csv or excel file


*for example (if you are in this folder): *
```py
   python3 stats.py ../apps/app_completed.py ../platforms/platform_completed.xml log.txt Sending Receiving Saving stats.csv
```