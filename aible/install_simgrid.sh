#!/bin/bash
# this file has been created to fix simgrid installation

apt install cmake
apt install libboost-dev
apt install libboost-context-dev
git clone https://framagit.org/simgrid/simgrid.git
cd simgrid
cmake -DCMAKE_INSTALL_PREFIX=/opt/simgrid .
apt install python3-pip
make
make install
apt install  pybind11-dev
sudo apt install python3-dev
pip3 install simgrid
