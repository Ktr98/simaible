import os, sys, random

application = sys.argv[1]
platform = sys.argv[2]

locally = int(sys.argv[3])
remotely = int(sys.argv[4])
num_cloud_servers = sys.argv[5]

code_app = ''
code_patients=''
code_doctors=''
kines_doctors_clouds=''
zone_routes=''
clusters = '<cluster id="AS1_cb1" prefix="cb1-" suffix=".dc1.acloud.com" radical="1-'+num_cloud_servers+'" speed="50Gf" bw="2GBps" lat="100us" bb_bw="10GBps" bb_lat="100us"/>'+'\n	  <cluster id="AS1_cb2" prefix="cb2-" suffix=".dc1.acloud.com" radical="1-'+num_cloud_servers+'" speed="50Gf" bw="2GBps" lat="100us" bb_bw="10GBps" bb_lat="100us"/>'+'\n	  <cluster id="AS1_cb3" prefix="cb3-" suffix=".dc1.acloud.com" radical="1-'+num_cloud_servers+'" speed="50Gf" bw="2GBps" lat="100us" bb_bw="10GBps" bb_lat="100us"/>\n	  '

def complete(file, code, where):
	with open(file,'r') as fr:
		ch = fr.read()

	# p = position du premier couple d'octets de valeurs \r\n
	# p+2 est la position du premier octet juste après le couple \r\n
	p = ch.find(where)
	with open(file,'w') as fw:
		fw.write(ch[0:p])
		fw.write(code)
		fw.write(ch[p:])
	fr.close()
	fw.close()

if __name__ == '__main__':

	for i in range(1, locally+1):
		code_app += 'Actor.create("exo'+str(i)+'", Host.by_name("exo'+str(i)+'"), workflow)\n	'
		code_patients+='<zone id="block'+str(i)+'" routing="Floyd">'+'\n	  <host id="emg'+str(i)+'" speed="1Gf"/>'+'\n	  <host id="exo'+str(i)+'" speed="5Gf"/>'+'\n	  <host id="lcn'+str(i)+'" speed="10Gf"/>'+'\n	  <host id="kine_hri'+str(i)+'" speed="10Gf"/>'+'\n	  <link id="emg'+str(i)+'_lcn'+str(i)+'" bandwidth="640MBps" latency="5ms"/>'+'\n	  <link id="exo'+str(i)+'_lcn'+str(i)+'" bandwidth="100MBps" latency="5ms"/>'+'\n	  <link id="kine_hri'+str(i)+'_lcn'+str(i)+'" bandwidth="100MBps" latency="5ms"/>'+'\n	  <route src="emg'+str(i)+'" dst="lcn'+str(i)+'">'+'\n		<link_ctn id="emg'+str(i)+'_lcn'+str(i)+'"/>'+'\n	  </route>'+'\n	  <route src="exo'+str(i)+'" dst="lcn'+str(i)+'">'+'\n		<link_ctn id="exo'+str(i)+'_lcn'+str(i)+'"/>'+'\n	  </route>'+'\n	  <route src="lcn'+str(i)+'" dst="kine_hri'+str(i)+'">'+'\n		<link_ctn id="kine_hri'+str(i)+'_lcn'+str(i)+'"/>'+'\n	  </route>'+'\n	</zone>\n	'
		kines_doctors_clouds+='<link id="kine_hri'+str(i)+'_router_AS1_dc1" bandwidth="20MBps" latency="5ms"/>\n	'
		zone_routes+='<zoneRoute src="block'+str(i)+'" dst="AS1_dc1" gw_src="kine_hri'+str(i)+'" gw_dst="router_AS1_dc1">'+'\n	  <link_ctn id="kine_hri'+str(i)+'_router_AS1_dc1"/>'+'\n	</zoneRoute>\n	'
		
	for i in range(1, remotely+1):
		code_doctors+='<zone id="doctor'+str(i)+'" routing="Full">'+'\n	  <host id="doc_hri'+str(i)+'" speed="50Mf"/>'+'\n	</zone>\n	'	
		kines_doctors_clouds+='<link id="doc_hri'+str(i)+'_router_AS1_dc1" bandwidth="20MBps" latency="5ms"/>\n	'
		zone_routes+='<zoneRoute src="doctor'+str(i)+'" dst="AS1_dc1" gw_src="doc_hri'+str(i)+'" gw_dst="router_AS1_dc1">'+'\n	  <link_ctn id="doc_hri'+str(i)+'_router_AS1_dc1"/>'+'\n	</zoneRoute>\n	'

	# apps completions
	complete(application, code_app, '# end code app')

	# platform completions
	complete(platform, code_patients, '<!-- end_patients_zone -->')
	complete(platform, code_doctors, '<!-- end_doctors_zone -->')
	complete(platform, kines_doctors_clouds, '<!-- end_kines_doctors_clouds_bindings -->')
	complete(platform, zone_routes, '<!-- end_zone_routes -->')
	complete(platform, clusters, '<!-- end_clusters -->')




