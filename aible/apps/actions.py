from simgrid import Mailbox, this_actor
from random import randint


def send(receiver, queries):
	for i in range(queries):
		#this_actor.info("Sending")
		message = str(randint(1, 1000))
		mbr = Mailbox.by_name(receiver)
		mbr.put(message, len(message))
		#this_actor.info("Sent : "+message)

def forward(receiver, queries):
	for i in range(queries):
		#this_actor.info("Forwarding")
		mbc = Mailbox.by_name(this_actor.get_host().name)
		message1 = mbc.get()
		message2 = str(randint(1, 1000))
		message1 = message1+"|"+message2
		mbr = Mailbox.by_name(receiver)
		mbr.put(message1, len(message1))
		#this_actor.info("Forwarded : "+message1)

def confirm(queries):
	for i in range(queries):
		#this_actor.info("Getting")
		mbc = Mailbox.by_name(this_actor.get_host().name)
		message = mbc.get()
		#this_actor.info("Got : "+message)
