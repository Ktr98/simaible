from simgrid import Engine, Actor, Host, this_actor
import os, sys
from random import randint
from actions import send, forward, confirm
from time import time

number_cloud_servers = int(sys.argv[1])
number_doctor = int(sys.argv[2])
queries = int(sys.argv[3])

def workflow():
	clt ="2"
	ccn1 = "cb"+clt+"-"+str(randint(1, number_cloud_servers))+".dc1.acloud.com"
	ccn2 = "cb"+clt+"-"+str(randint(1, number_cloud_servers))+".dc1.acloud.com"

	exo_host = this_actor.get_host()
	exo_name = exo_host.name

	exo_atcor1 = Actor.create(exo_name, exo_host, send, "lcn"+exo_name[3:len(exo_name)], queries)
	lcn_actor1 = Actor.create("lcn"+exo_name[3:len(exo_name)], Host.by_name("lcn"+exo_name[3:len(exo_name)]), forward, "kine_hri"+exo_name[3:len(exo_name)], queries)
	kine_actor1 = Actor.create("kine_hri"+exo_name[3:len(exo_name)], Host.by_name("kine_hri"+exo_name[3:len(exo_name)]), forward, ccn1, queries)
	ccn_actor1 = Actor.create(ccn1, Host.by_name(ccn1), forward, "doc_hri"+str(randint(1, number_doctor)), queries)
	doc_actor1 = Actor.create("doc_hri"+str(randint(1, number_doctor)), Host.by_name("doc_hri"+str(randint(1, number_doctor))), confirm, queries)
	
	percentage = int(randint(1, 101)//100)
	kine_actor2 = Actor.create("kine_hri"+exo_name[3:len(exo_name)], Host.by_name("kine_hri"+exo_name[3:len(exo_name)]), send, "lcn"+exo_name[3:len(exo_name)], percentage*queries)
	kine_actor3 = Actor.create("kine_hri"+exo_name[3:len(exo_name)], Host.by_name("kine_hri"+exo_name[3:len(exo_name)]), send, ccn2, percentage*queries)
	lcn_actor2 = Actor.create("lcn"+exo_name[3:len(exo_name)], Host.by_name("lcn"+exo_name[3:len(exo_name)]), forward, exo_name, percentage*queries)
	ccn_actor2 = Actor.create(ccn2, Host.by_name(ccn2), forward, "doc_hri"+str(randint(1, number_doctor)), percentage*queries)
	exo_atcor1 = Actor.create(exo_name, exo_host, confirm, percentage*queries)
	doc_actor2 = Actor.create("doc_hri"+str(randint(1, number_doctor)), Host.by_name("doc_hri"+str(randint(1, number_doctor))), confirm, percentage*queries)




if __name__ == "__main__":
	
	e = Engine(sys.argv)

	e.load_platform("platform.xml")

	# code app
	Actor.create("exo1", Host.by_name("exo1"), workflow)
	Actor.create("exo2", Host.by_name("exo2"), workflow)
	Actor.create("exo3", Host.by_name("exo3"), workflow)
	Actor.create("exo4", Host.by_name("exo4"), workflow)
	Actor.create("exo5", Host.by_name("exo5"), workflow)
	Actor.create("exo6", Host.by_name("exo6"), workflow)
	Actor.create("exo1", Host.by_name("exo1"), workflow)
	Actor.create("exo2", Host.by_name("exo2"), workflow)
	Actor.create("exo3", Host.by_name("exo3"), workflow)
	Actor.create("exo4", Host.by_name("exo4"), workflow)
	Actor.create("exo5", Host.by_name("exo5"), workflow)
	Actor.create("exo6", Host.by_name("exo6"), workflow)
	Actor.create("exo1", Host.by_name("exo1"), workflow)
	Actor.create("exo2", Host.by_name("exo2"), workflow)
	Actor.create("exo3", Host.by_name("exo3"), workflow)
	Actor.create("exo4", Host.by_name("exo4"), workflow)
	Actor.create("exo5", Host.by_name("exo5"), workflow)
	Actor.create("exo6", Host.by_name("exo6"), workflow)
	Actor.create("exo1", Host.by_name("exo1"), workflow)
	Actor.create("exo2", Host.by_name("exo2"), workflow)
	Actor.create("exo3", Host.by_name("exo3"), workflow)
	Actor.create("exo4", Host.by_name("exo4"), workflow)
	Actor.create("exo5", Host.by_name("exo5"), workflow)
	Actor.create("exo6", Host.by_name("exo6"), workflow)
	# end code app
	start = time()
	e.run()
	end = time()
	#this_actor.info("Simulation time {}".format(Engine.get_clock()))
	#this_actor.info("Simulation time(s) {}".format(end-start))
	print(end-start)
