#!/bin/bash

for locally in "1" "2" "3" "4" "5"
do
	for num_cloud_servers in "1" "2" "3" "4" "5" "6" "7" "8" "9" "10" 
	do
		res=`./run.sh apps/dvrg.py platform.xml $locally 1 $num_cloud_servers 14400000`
		echo $num_cloud_servers";"$res >> "stats/"$locally"-local.csv"
	done
done
