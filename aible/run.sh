#!/bin/bash

application=$1
platform=$2
locally=$3
remotely=$4
num_cloud_servers=$5
queries=$6

cp $platform tmp.xml 
cp $application tmp.py

python3 update.py $application $platform $locally $remotely $num_cloud_servers

python3 $application $num_cloud_servers $remotely $queries

cp tmp.xml $platform
cp tmp.py $application

rm tmp.*
