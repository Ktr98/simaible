file(REMOVE_RECURSE
  "bin/colorize"
  "bin/simgrid_update_xml"
  "bin/smpicc"
  "bin/smpicxx"
  "bin/smpif90"
  "bin/smpiff"
  "bin/smpirun"
  "examples/smpi/tracing/smpi_traced.trace"
  "include/simgrid/config.h"
  "include/simgrid/version.h"
  "include/smpi/mpif.h"
  "src/internal_config.h"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/JavaContext.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_as.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_comm.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_file.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_host.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_process.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_storage.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_synchro.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_task.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jmsg_vm.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jtrace.cpp.o"
  "CMakeFiles/simgrid-java.dir/src/bindings/java/jxbt_utilities.cpp.o"
  "lib/libsimgrid-java.pdb"
  "lib/libsimgrid-java.so"
  "lib/libsimgrid-java.so.3.25"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/simgrid-java.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
