# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.19

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Disable VCS-based implicit rules.
% : %,v


# Disable VCS-based implicit rules.
% : RCS/%


# Disable VCS-based implicit rules.
% : RCS/%,v


# Disable VCS-based implicit rules.
% : SCCS/s.%


# Disable VCS-based implicit rules.
% : s.%


.SUFFIXES: .hpux_make_needs_suffix_list


# Command-line flag to silence nested $(MAKE).
$(VERBOSE)MAKESILENT = -s

#Suppress display of executed commands.
$(VERBOSE).SILENT:

# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /snap/cmake/805/bin/cmake

# The command to remove a file.
RM = /snap/cmake/805/bin/cmake -E rm -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/lyan/Téléchargements/SimGrid-3.25

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/lyan/Téléchargements/SimGrid-3.25

# Utility rule file for uninstall.

# Include the progress variables for this target.
include CMakeFiles/uninstall.dir/progress.make

CMakeFiles/uninstall:
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/doc/simgrid
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E echo uninstall\ doc\ ok
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/lib/libsimgrid*
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/lib/lua/5.1/simgrid*
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E echo uninstall\ lib\ ok
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/smpicc
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/smpicxx
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/smpiff
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/smpif90
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/smpirun
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/tesh
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/simgrid-colorizer
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/simgrid_update_xml
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/simgrid_convert_TI_traces
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/bin/graphicator
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E echo uninstall\ bin\ ok
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/instr
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/msg
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/simdag
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/smpi
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/simix
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/surf
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/xbt
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/mc
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove_directory /opt/simgrid/include/simgrid
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/include/simgrid.h
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/include/simgrid/config.h
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/include/xbt.h
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E echo uninstall\ include\ ok
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/share/man/man1/simgrid_update_xml.1
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/share/man/man1/tesh.1
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/share/man/man1/smpicc.1
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/share/man/man1/smpicxx.1
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/share/man/man1/smpirun.1
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/share/man/man1/smpiff.1
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E remove -f /opt/simgrid/share/man/man1/smpif90.1
	cd /opt/simgrid && /snap/cmake/805/bin/cmake -E echo uninstall\ man\ ok

uninstall: CMakeFiles/uninstall
uninstall: CMakeFiles/uninstall.dir/build.make

.PHONY : uninstall

# Rule to build all files generated by this target.
CMakeFiles/uninstall.dir/build: uninstall

.PHONY : CMakeFiles/uninstall.dir/build

CMakeFiles/uninstall.dir/clean:
	$(CMAKE_COMMAND) -P CMakeFiles/uninstall.dir/cmake_clean.cmake
.PHONY : CMakeFiles/uninstall.dir/clean

CMakeFiles/uninstall.dir/depend:
	cd /home/lyan/Téléchargements/SimGrid-3.25 && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/lyan/Téléchargements/SimGrid-3.25 /home/lyan/Téléchargements/SimGrid-3.25 /home/lyan/Téléchargements/SimGrid-3.25 /home/lyan/Téléchargements/SimGrid-3.25 /home/lyan/Téléchargements/SimGrid-3.25/CMakeFiles/uninstall.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : CMakeFiles/uninstall.dir/depend

