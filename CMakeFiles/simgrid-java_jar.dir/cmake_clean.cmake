file(REMOVE_RECURSE
  "bin/colorize"
  "bin/simgrid_update_xml"
  "bin/smpicc"
  "bin/smpicxx"
  "bin/smpif90"
  "bin/smpiff"
  "bin/smpirun"
  "examples/smpi/tracing/smpi_traced.trace"
  "include/simgrid/config.h"
  "include/simgrid/version.h"
  "include/smpi/mpif.h"
  "src/internal_config.h"
  "CMakeFiles/simgrid-java_jar"
  "CMakeFiles/simgrid-java_jar.dir/java_class_filelist"
  "CMakeFiles/simgrid-java_jar.dir/java_compiled_simgrid-java_jar"
  "simgrid.jar"
)

# Per-language clean rules from dependency scanning.
foreach(lang )
  include(CMakeFiles/simgrid-java_jar.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
