file(REMOVE_RECURSE
  "bin/colorize"
  "bin/simgrid_update_xml"
  "bin/smpicc"
  "bin/smpicxx"
  "bin/smpif90"
  "bin/smpiff"
  "bin/smpirun"
  "examples/smpi/tracing/smpi_traced.trace"
  "include/simgrid/config.h"
  "include/simgrid/version.h"
  "include/smpi/mpif.h"
  "src/internal_config.h"
  "CMakeFiles/smpireplaymain.dir/src/smpi/smpi_replay_main.cpp.o"
  "lib/simgrid/smpireplaymain"
  "lib/simgrid/smpireplaymain.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/smpireplaymain.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
