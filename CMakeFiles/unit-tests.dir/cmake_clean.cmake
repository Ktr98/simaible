file(REMOVE_RECURSE
  "bin/colorize"
  "bin/simgrid_update_xml"
  "bin/smpicc"
  "bin/smpicxx"
  "bin/smpif90"
  "bin/smpiff"
  "bin/smpirun"
  "examples/smpi/tracing/smpi_traced.trace"
  "include/simgrid/config.h"
  "include/simgrid/version.h"
  "include/smpi/mpif.h"
  "src/internal_config.h"
  "CMakeFiles/unit-tests.dir/src/kernel/lmm/maxmin_test.cpp.o"
  "CMakeFiles/unit-tests.dir/src/kernel/resource/profile/Profile_test.cpp.o"
  "CMakeFiles/unit-tests.dir/src/xbt/config_test.cpp.o"
  "CMakeFiles/unit-tests.dir/src/xbt/dict_test.cpp.o"
  "CMakeFiles/unit-tests.dir/src/xbt/dynar_test.cpp.o"
  "CMakeFiles/unit-tests.dir/src/xbt/random_test.cpp.o"
  "CMakeFiles/unit-tests.dir/src/xbt/unit-tests_main.cpp.o"
  "CMakeFiles/unit-tests.dir/src/xbt/xbt_str_test.cpp.o"
  "unit-tests"
  "unit-tests.pdb"
)

# Per-language clean rules from dependency scanning.
foreach(lang CXX)
  include(CMakeFiles/unit-tests.dir/cmake_clean_${lang}.cmake OPTIONAL)
endforeach()
