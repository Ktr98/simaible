# CMake generated Testfile for 
# Source directory: /home/lyan/Téléchargements/SimGrid-3.25/teshsuite/mc
# Build directory: /home/lyan/Téléchargements/SimGrid-3.25/teshsuite/mc
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(mc-random-bug-replay "/usr/bin/python3" "/home/lyan/Téléchargements/SimGrid-3.25/bin/tesh" "--ignore-jenkins" "--setenv" "platfdir=/home/lyan/Téléchargements/SimGrid-3.25/examples/platforms" "--setenv" "bindir=/home/lyan/Téléchargements/SimGrid-3.25/teshsuite/mc/random-bug" "--cd" "/home/lyan/Téléchargements/SimGrid-3.25/teshsuite/mc/random-bug" "random-bug-replay.tesh")
set_tests_properties(mc-random-bug-replay PROPERTIES  _BACKTRACE_TRIPLES "/home/lyan/Téléchargements/SimGrid-3.25/tools/cmake/Tests.cmake;49;ADD_TEST;/home/lyan/Téléchargements/SimGrid-3.25/teshsuite/mc/CMakeLists.txt;52;ADD_TESH;/home/lyan/Téléchargements/SimGrid-3.25/teshsuite/mc/CMakeLists.txt;0;")
