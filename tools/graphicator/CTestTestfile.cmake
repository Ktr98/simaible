# CMake generated Testfile for 
# Source directory: /home/lyan/Téléchargements/SimGrid-3.25/tools/graphicator
# Build directory: /home/lyan/Téléchargements/SimGrid-3.25/tools/graphicator
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(graphicator "/usr/bin/python3" "/home/lyan/Téléchargements/SimGrid-3.25/bin/tesh" "--ignore-jenkins" "--setenv" "srcdir=/home/lyan/Téléchargements/SimGrid-3.25" "--setenv" "bindir=/home/lyan/Téléchargements/SimGrid-3.25/bin" "--cd" "/home/lyan/Téléchargements/SimGrid-3.25/tools/graphicator" "/home/lyan/Téléchargements/SimGrid-3.25/tools/graphicator/graphicator.tesh")
set_tests_properties(graphicator PROPERTIES  _BACKTRACE_TRIPLES "/home/lyan/Téléchargements/SimGrid-3.25/tools/cmake/Tests.cmake;49;ADD_TEST;/home/lyan/Téléchargements/SimGrid-3.25/tools/graphicator/CMakeLists.txt;6;ADD_TESH;/home/lyan/Téléchargements/SimGrid-3.25/tools/graphicator/CMakeLists.txt;0;")
